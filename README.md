# Epic: k8s platform and integration

1. US1: Deploy Jenkins on Microk8s
2. US2: Deploy mysql and phpmyadmin app.
3. US3: Deploy prometheus and grafana.
4. US4: Deploy sonarqube along with postgres.
